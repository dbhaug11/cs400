package haugenBudinKorver.Scheme;

import java.util.LinkedList;
import java.util.List;

public class Filter implements Procedure {

	@Override
	public int minArgs() {
		return 2;
	}

	@Override
	public boolean varArg() {
		return false;
	}

	@Override
	public Object apply(List<Object> params) throws Exception {

		List<Object> lst = new LinkedList<Object>();
		if (params.get(0) instanceof Lambda && params.get(1) instanceof List<?>) {
			Lambda l = (Lambda) params.get(0);
			Environment env = l.getEnv();
			Symbol par = l.getParams().get(0);
			@SuppressWarnings("unchecked")
			List<Object> lstObj = (List<Object>) params.get(1);

			for (int i = 0; i < lstObj.size(); i++) {
				env.add(par, lstObj.get(i));
				Object filt = Utils.eval(l.getCode(), env, new Store());
				if (filt instanceof Boolean && (Boolean) filt) {
					lst.add(lstObj.get(i));
				}
			}
		} else {
			throw new Exception("Invalid syntax: (filter ( _a -> boolean ) (listof _a))");
		}
		return lst;
	}

}
