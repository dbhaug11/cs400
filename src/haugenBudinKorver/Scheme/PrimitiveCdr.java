package haugenBudinKorver.Scheme;

import java.util.List;

public class PrimitiveCdr implements Procedure {

	@Override
	public int minArgs() {
		return 1;
	}

	@Override
	public boolean varArg() {
		return false;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object apply(List<Object> params) throws Exception {
		Object temp=params.get(0);
		if(temp instanceof String){
			return ((String) temp).substring(1);
		}else if(temp instanceof List){
			return ((List) temp).subList(1, ((List) temp).size());
		}
		throw new Exception("Wrong argument for cdr"+temp);
	}

}
