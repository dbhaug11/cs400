package haugenBudinKorver.Scheme;

import java.util.LinkedList;
import java.util.List;

public class Utils {
	private static Object ifForm(List<?> lst, Environment e, Store s) throws Exception {
		Object condition = lst.get(1);
		condition = eval(condition, e, s);
		if (!(condition instanceof Boolean)) {
			throw new Exception("Condition did not return Boolean: "
					+ condition);
		} else {
			if ((Boolean) condition) {
				return lst.get(2);
			} else {
				return lst.get(3);
			}
		}
	}

	private static Object beginForm(List<?> lst, Environment e, Store s)
			throws Exception {
		if (lst.size() <= 1) {
			throw new Exception("Begin must have arguments");
		} else if (lst.size() == 2) {
			return lst.get(1);
		} else {
			eval(lst.get(1), e, s);
			LinkedList<Object> code = new LinkedList<Object>();
			code.add(Symbol.getSymbol("begin"));
			for (int i = 2; i < lst.size(); i++) {
				code.add(lst.get(i));
			}
			return code;
		}
	}

	private static Object lambdaForm(List<?> lst, Environment e, Store s)
			throws Exception {
		LinkedList<Object> code = new LinkedList<Object>();
		code.add(Symbol.getSymbol("begin"));
		for (int i = 2; i < lst.size(); i++) {
			code.add(lst.get(i));
		}
		List<Symbol> params = new LinkedList<Symbol>();
		Object codeArgs = lst.get(1);
		if (!(codeArgs instanceof List<?>)) {
			throw new Exception(
					"Parameters for lambda must be a list of Symbols.");
		}

		lst = (List<?>) codeArgs;

		for (int i = 0; i < lst.size(); i++) {
			if (lst.get(i) instanceof Symbol) {
				params.add((Symbol) lst.get(i));
			} else {
				throw new Exception("Expected Symbol, got " + lst.get(i));
			}
		}
		return new Lambda(e, code, params);
	}

	private static void defineForm(List<?> lst, Environment e, Store s) throws Exception {
		Symbol name;

		if (lst.get(1) instanceof List<?>) {

			List<?> signature = (List<?>) lst.get(1);

			name = (Symbol) signature.get(0);

			LinkedList<Symbol> params = new LinkedList<Symbol>();
			if (signature.size() > 1) {
				for (int i = 1; i < signature.size(); i++) {
					params.add((Symbol) signature.get(i));
				}
			}

			LinkedList<Object> code = new LinkedList<Object>();
			code.add(Symbol.getSymbol("begin"));
			for (int i = 2; i < lst.size(); i++) {
				code.add(lst.get(i));
			}

			Lambda expr = new Lambda(e, code, params);

			e.add(name, expr);

		} else if (lst.get(1) instanceof Symbol) {

			name = (Symbol) lst.get(1);
			Object value = eval(lst.get(2), e, s);

			e.add(name, value);

		} else {

			throw new Exception(
					"Define expects symbol or list as first argument.");
		}
	}

	public static Object eval(Object o, Environment e, Store s) throws Exception {
		while (true) {

			if (o instanceof Number || o instanceof Boolean) {

				// numbers, booleans, and strings are
				// self-evaluating

				return o;

			} else if (o instanceof Symbol) {

				// symbols need to be looked up in
				// the environment
				if(((Symbol) o).getValue().charAt(0) == '\''){
					return o;
				}
				return e.get((Symbol) o);

			} else if (o instanceof List<?>) {

				// we cast o to List<?> because the parameter
				// type is lost at runtime. all we can
				// verify is that it's a list, which is all
				// that we care about anyway
				List<?> lst = (List<?>) o;
				
//				for(Iterator i=lst.iterator();i.hasNext();){
//					System.out.println(i.next());
//				}
				if(lst.get(0) instanceof Symbol){
					if(((Symbol) lst.get(0)).getValue().toCharArray()[0]=='\'' ){
						return lst;
					}
				}
				// the input is a list, so we need to check
				// for the special cases and potentially
				// treat the list as a function call
				Object root = lst.get(0);

				if (root instanceof Symbol) {
					// if the first element of the list
					// (the "car") is a Symbol, we
					// need to check for the special cases
					Symbol car = (Symbol) root;
					if (car.equals(Symbol.getSymbol("if"))) {

						// throw error if wrong number
						// of args
						if (lst.size() != 4) {
							throw new Exception("If expects three args.");
						}

						// *change* the code and
						// *don't* return
						o = ifForm(lst, e, s);
						continue; // jump to top of loop!

					} else if (car.equals(Symbol.getSymbol("lambda"))) {

						// throw error if wrong number
						// of args
						if (lst.size() != 3) {
							throw new Exception("Lambda expects two args.");
						}

						// return the new lambda expression
						return lambdaForm(lst, e, s);
					} else if (car.equals(Symbol.getSymbol("begin"))) {

						// (begin a b) becomes (eval a)
						// then (begin b)

						o = beginForm(lst, e, s);
						continue;

					} else if (car.equals(Symbol.getSymbol("define"))) {

						// throw error if wrong number
						// of args
						if (lst.size() != 3) {
							throw new Exception("Define expects two args.");
						}

						defineForm(lst, e, s);
						return null;
						// define returns nothing

					} else if (car.equals(Symbol.getSymbol("quote"))) {

						// throw error if wrong number
						// of args
						if (lst.size() != 2) {
							throw new Exception("Quote expects one arg.");
						}

						// return the "quoted"
						// element without evaling it
						return lst.get(1);

					}
				}

				// We've checked for all the special
				// forms, so this list must either
				// be a function call or an error.

				// First, we need to make sure the
				// first element of the list is
				// a function. That means eval'ing it.
				// If it's a symbol corresponding
				// to a function, for example, we
				// need to eval that symbol to get
				// the function.
				root = eval(root, e, s);

				if (root instanceof Procedure) {
					// running a built-in function
					// create a parameter list
					List<Object> params = new LinkedList<Object>();
					for (int i = 1; i < lst.size(); i++) {
						// eval each of the parameters
						params.add(eval(lst.get(i), e, s));
					}
					/*
					 * Big ugly conditional checks to make sure the parameter
					 * counts line up. Built-in procedures can have a variable
					 * number of parameters.
					 */
					if (( params.size() >= ((Procedure) root).minArgs() && ((Procedure) root)
									.varArg())||params.size()==((Procedure) root).minArgs()) {
						return ((Procedure) root).apply(params);
					}else{
						// argument count didn't line up
						//System.out.println(root.getClass());
						throw new Exception("Wrong arguments for " + root);
					}
				}

				else if (!(root instanceof Lambda)) {
					throw new Exception("Expected function, found " + root);
				}

				Lambda func = (Lambda) root;
				Environment newEnv = new Environment(func.getEnv());

				if (lst.size() != func.getParams().size() + 1) {
					throw new Exception("Wrong number of arguments: " + o);
				}

				// eval all the parameters
				List<Symbol> params = func.getParams();
				for (int i = 1; i < lst.size(); i++) {
					newEnv.add(params.get(i - 1), eval(lst.get(i), e, s));
				}

				// replace the code and environment,
				// then loop to the top
				e = newEnv;
				o = func.getCode();
			}
		}
	}

	static Environment registerBuiltins() {
		Environment e = new Environment();
		e.add(Symbol.getSymbol("+"), new PrimitiveAdd());
		e.add(Symbol.getSymbol("*"), new PrimitiveMultiply());
		e.add(Symbol.getSymbol("-"), new PrimitiveSubtract());
		e.add(Symbol.getSymbol("/"), new PrimitiveDivide());
		e.add(Symbol.getSymbol("="), new PrimitiveEquals());
		e.add(Symbol.getSymbol(">"), new PrimitiveGT());
		e.add(Symbol.getSymbol("<"), new PrimitiveLT());
		e.add(Symbol.getSymbol("display"), new PrimitiveDisplay());
		e.add(Symbol.getSymbol("cons"), new PrimitiveCons());
		e.add(Symbol.getSymbol("car"), new PrimitiveCar());
		e.add(Symbol.getSymbol("cdr"), new PrimitiveCdr());
		e.add(Symbol.getSymbol("null?"), new PrimitiveNull());
		e.add(Symbol.getSymbol("map"), new Map());
		e.add(Symbol.getSymbol("filter"), new Filter());
		e.add(Symbol.getSymbol("reduce"), new Reduce());
		e.add(Symbol.getSymbol("nil"), null);
		return e;
	}

}