package haugenBudinKorver.Scheme;

import java.util.List;

public class PrimitiveGT implements Procedure{

	@Override
	public int minArgs() {
		return 2;
	}

	@Override
	public boolean varArg() {
		return false;
	}

	@Override
	public Object apply(List<Object> params) throws Exception {
		if(params.get(0) instanceof Number && params.get(1) instanceof Number){
			return ((Number) params.get(0)).doubleValue()>((Number) params.get(1)).doubleValue();
		}
		return false;
	}
	
}
