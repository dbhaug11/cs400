package haugenBudinKorver.Scheme;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

public abstract class PrimitiveBinop implements Procedure {
	public Object apply(List<Object> params) throws Exception {
		Iterator<Object> iter = params.iterator();
		BigDecimal res= new BigDecimal(iter.next().toString());
        while (iter.hasNext()) {
            res=innerApply(res, iter);
        }
        return res;
	}
	
	@Override
	public int minArgs() {
		return 2;
	}

	@Override
	public boolean varArg() {
		return true;
	}
	
	abstract BigDecimal innerApply(BigDecimal res, Iterator<Object> iter);
}
