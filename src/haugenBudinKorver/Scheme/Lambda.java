package haugenBudinKorver.Scheme;

import java.util.List;

public class Lambda {
    private Environment env;
    private Object code;
    private List<Symbol> params;
 
    public Lambda(Environment env, Object code, List<Symbol> params) {
        this.env = env;
        this.code = code;
        this.params = params;
    }
 
    public Environment getEnv() {
        return env;
    }
 
    public Object getCode() {
        return code;
    }
 
    public List<Symbol> getParams() {
        return params;
    }
 
    public String toString() {
        return "\\" + params + " -> " + code;
    }
 
}