package haugenBudinKorver.Scheme;

import java.util.LinkedList;
import java.util.List;

public class PrimitiveCons implements Procedure{

	@Override
	public int minArgs() {
		return 2;
	}

	@Override
	public boolean varArg() {
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object apply(List<Object> params) throws Exception {
		((LinkedList<Object>) params.get(1)).add(params.get(0));
		return params.get(1);
	}	
}
