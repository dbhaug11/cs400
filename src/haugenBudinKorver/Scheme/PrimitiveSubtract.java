package haugenBudinKorver.Scheme;

import java.math.BigDecimal;
import java.util.Iterator;

public class PrimitiveSubtract extends PrimitiveBinop {

	@Override
	BigDecimal innerApply(BigDecimal res, Iterator<Object> iter){
		return res.subtract((BigDecimal) iter.next());
	}
	
}