package haugenBudinKorver.Scheme;

import java.util.HashMap;

public class Store {
	private HashMap<Number, Object> sto;
	
	public Store (){
		sto=new HashMap<Number, Object>();
	}
	
	public void add(Number location, Object o){
		sto.put(location, o);
	}
	
	public Object get(Number location) throws Exception {
		if(sto.containsKey(location)){
			return sto.get(location);
		}else{
			throw new Exception (" Location not found :" + location);
		}
	}
	
	public void set(Number location, Object o){
		sto.put(location, o);
	}
	
	public String toString() {
		return sto.toString();
	}
}
