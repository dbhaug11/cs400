package haugenBudinKorver.Scheme;

import java.util.LinkedList;
import java.util.List;

public class Reduce implements Procedure {

	@Override
	public int minArgs() {
		return 2;
	}

	@Override
	public boolean varArg() {
		return false;
	}

	@Override
	public Object apply(List<Object> params) throws Exception {
		List<Object> lst = new LinkedList<Object>();
		if (params.get(0) instanceof Lambda && params.get(1) instanceof List<?>) {
			Lambda l = (Lambda) params.get(0);
			Environment env = l.getEnv();
			Symbol par0 = l.getParams().get(0);
			Symbol par1 = l.getParams().get(1);
			@SuppressWarnings("unchecked")
			List<Object> lstObj = (List<Object>) params.get(1);
			lst.add(lstObj.get(0));

			for (int i = 1; i < lstObj.size(); i++) {
				env.add(par0, lst.get(i-1));
				env.add(par1, lstObj.get(i));
				lst.add(Utils.eval(l.getCode(), env, new Store()));
			}
		} else {
			throw new Exception("Invalid syntax: (reduce ( _a _a -> _a) (listof _a))");
		}
		if(lst.isEmpty()){
			return lst;
		}
		return lst.get(lst.size()-1);
	}
	
}
