package haugenBudinKorver.Scheme;

import java.math.BigDecimal;
import java.util.Iterator;

public class PrimitiveMultiply extends PrimitiveBinop {

	@Override
	BigDecimal innerApply(BigDecimal res, Iterator<Object> iter){
		return res.multiply((BigDecimal) iter.next());
	}
	
}
