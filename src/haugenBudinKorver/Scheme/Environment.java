package haugenBudinKorver.Scheme;

import java.util.HashMap;

public class Environment {
    private Environment parent;
    private HashMap<Symbol, Object> env;
 
    public Environment(Environment parent) {
        this.parent = parent;
        env = new HashMap<Symbol, Object>();
    }
 
    public Environment() {
        parent = null;
        env = new HashMap<Symbol, Object>();
    }
 
    public void add(Symbol s, Object o) {
        env.put(s, o);
    }
 
    public Object get(Symbol s) throws Exception {
        if (env.containsKey(s)) {
            return env.get(s);
        } else {
            if (parent != null) {
                return parent.get(s);
            } else {
                throw new Exception("Symbol binding not found:" + s);
            }
        }
    }
 
    public void set(Symbol s, Object o) {
        if (env.containsKey(s)) {
            env.put(s, o);
        } else {
            if (parent != null) {
                parent.set(s, o);
            } else {
                env.put(s, o);
            }
        }
    }
 
    public String toString() {
        String str = env.toString();
        if (parent != null) {
            str += "\n" + parent;
        }
        return str;
    }
 
}
