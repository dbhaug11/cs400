package haugenBudinKorver.Scheme;

import java.math.BigDecimal;
import java.util.Iterator;

public class PrimitiveAdd extends PrimitiveBinop {
	
	@Override
	BigDecimal innerApply(BigDecimal res, Iterator<Object> iter){
		return res.add((BigDecimal) iter.next());
	}
}
