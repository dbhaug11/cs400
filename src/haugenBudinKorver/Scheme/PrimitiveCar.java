package haugenBudinKorver.Scheme;

import java.util.List;

public class PrimitiveCar implements Procedure{
	@Override
	public int minArgs() {
		return 1;
	}

	@Override
	public boolean varArg() {
		return false;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object apply(List<Object> params) throws Exception {
		Object temp=params.get(0);
		if(temp instanceof String){
			return ((String) temp).charAt(0);
		}else if(temp instanceof List){
			return ((List) temp).get(0);
		}
		System.out.println(temp.getClass());
		throw new Exception("Wrong argument for car"+temp);
	}
}
