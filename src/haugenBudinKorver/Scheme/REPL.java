package haugenBudinKorver.Scheme;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class REPL {
	private static Environment baseEnvironment = Utils.registerBuiltins();
	private static Store sto = new Store();
	private static String fullIn = null;

	public static void main(String[] args) throws Exception {
		Parser p = new Parser();
		Scanner in = new Scanner(System.in);
		List<Object> code, result;
		code = null;
		Iterator<Object> iter;
		Environment env = new Environment(baseEnvironment);
		Procedure printer = new PrimitiveDisplay();
		fullIn = "";
		System.out.print("> ");
		while (in.hasNextLine()) {
			try {
				fullIn += ' ';
				fullIn += in.nextLine();
				code = p.startParser(fullIn);
				if (p.getParncntdedetnum() > 0) {
					System.out.print("  ");
					for(int i=0; i<p.getParncntdedetnum();i++){
						System.out.print("  ");
					}
					continue;
				} else if (p.getParncntdedetnum() < 0) {
					throw new Exception("Parse error: invalid syntax");
				}

				fullIn = "";
				iter = code.iterator();
				while (iter.hasNext()) {

					result = new LinkedList<Object>();

					result.add(Utils.eval(iter.next(), env, sto));

					printer.apply(result);

				}
			} catch (Exception e) {
				List<Object> error = new LinkedList<Object>();
				error.add("Error!: " + e.getMessage());
				printer.apply(error);
				fullIn = "";
			}
			System.out.print("> ");
		}
		in.close();
	}
}
