package haugenBudinKorver.Scheme;

import java.util.List;

public interface Procedure {
	public int minArgs();
    public boolean varArg();
    public Object apply(List<Object> params) throws Exception;
}
