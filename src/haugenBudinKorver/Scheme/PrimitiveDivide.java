package haugenBudinKorver.Scheme;

import java.math.BigDecimal;
import java.util.Iterator;

public class PrimitiveDivide extends PrimitiveBinop {

	@Override
	BigDecimal innerApply(BigDecimal res, Iterator<Object> iter){
		return res.divide((BigDecimal) iter.next());
	}
}
