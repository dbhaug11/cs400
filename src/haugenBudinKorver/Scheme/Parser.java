package haugenBudinKorver.Scheme;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class Parser {
	private int ptr;
	private String in;
	// Name courtesy of Racket naming system
	private int parncntdedetnum;

	private void match(char c) throws Exception {
		if (in.charAt(ptr) == c) {
			ptr++;
		} else {
			throw new Exception("Invalid character at " + ptr + ": " + c);
		}
	}

	private boolean validAtomChar(char c) {
		return c != ' ' && c != '\n' && c != '\t' && c != '(' && c != ')'
				&& c != '#' && c != '[' && c != ']';
	}

	private boolean endOfString() {
		return ptr == in.length();
	}

	private void eatWhitespace() throws Exception {
		char c = cur();
		while (c == ' ' || c == '\n' || c == '\t') {
			match(c);
			c = cur();
		}
	}

	private char cur() throws Exception {
		if (ptr < in.length()) {
			return in.charAt(ptr);
		} else if (parncntdedetnum > 0) {
			return '\0';
		} else {
			throw new Exception("Invalid Syntax");
		}
	}

	private boolean isNumber(String s) {
		return s.matches("(-|\\+)?[0-9]+(\\.[0-9]+)?");
	}

	private List<Object> parseList() throws Exception {
		// create data structure
		LinkedList<Object> lst = new LinkedList<Object>();

		match('(');
		eatWhitespace();
		if (cur() != '\0') {

			while (cur() != ')') {
				// loop until finding an end paren

				if (cur() == '(') {
					// nested list
					parncntdedetnum++;
					lst.add(parseList());
				} else {
					Object temp = parseAtom();
					if (temp == Symbol.getSymbol("")) {
						return lst;
					}
					lst.add(temp);
				}
				eatWhitespace();
			}

			match(')');

			parncntdedetnum--;

			return lst;
		}
		return lst;
	}

	private Object parseAtom() throws Exception {
		String buf = "";
		while (!endOfString() && validAtomChar(cur())) {
			// build buffer
			buf += cur();
			match(cur());
		}
		// determine what's in the buffer
		if (isNumber(buf)) {
			return new BigDecimal(buf);
		} else {
			return Symbol.getSymbol(buf);
		}
	}

	public List<Object> startParser(String s) throws Exception {
		ptr = 0;
		in = s;
		parncntdedetnum = 0;
		eatWhitespace();
		LinkedList<Object> lst = new LinkedList<Object>();
		while (ptr < in.length()) {
			if (cur() != '\0' && cur() == ')' && parncntdedetnum <= 0) {
				throw new Exception("Unexpected parenthesis");
			}
			if (cur() != '\0' && cur() == '(') {
				parncntdedetnum++;
				lst.add(parseList());
			} else {
				Object temp = parseAtom();
				if (temp == Symbol.getSymbol("")) {
					return lst;
				}
				lst.add(temp);
			}
			if (ptr != in.length()) {
				eatWhitespace();
			}

		}
		return lst;
	}

	public int getParncntdedetnum() {
		return parncntdedetnum;
	}
}
