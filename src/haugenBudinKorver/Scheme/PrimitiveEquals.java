package haugenBudinKorver.Scheme;

import java.util.List;

public class PrimitiveEquals implements Procedure {

	@Override
	public int minArgs() {
		return 2;
	}

	@Override
	public boolean varArg() {
		return false;
	}

	@Override
	public Object apply(List<Object> params) throws Exception {
		return params.get(0).equals(params.get(1));
	}
}
