package haugenBudinKorver.Scheme;

import java.util.List;

public class PrimitiveNull implements Procedure {

	@Override
	public int minArgs() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public boolean varArg() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object apply(List<Object> params) throws Exception {
		if(params.get(0) instanceof List){
			return ((List) params.get(0)).size()==0;
		}
		return params.get(0)==null;
	}
}
