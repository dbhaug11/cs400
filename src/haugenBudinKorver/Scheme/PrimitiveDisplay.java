package haugenBudinKorver.Scheme;

import java.util.Iterator;
import java.util.List;

public class PrimitiveDisplay implements Procedure {
	public Object apply(List<Object> params) {
		Iterator<Object> iter = params.iterator();
		while(iter.hasNext()){
			System.out.println(iter.next().toString());
		}
		return "";
	}	
	public int minArgs() {
		return 1;
	}
		
	public boolean varArg() {
		return true;
	}
}
